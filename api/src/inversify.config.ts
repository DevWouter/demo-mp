import { Container } from "inversify";
import { Program } from "./program";

import { ContentModule } from "./content/content.module";

let container = new Container();
container.load(ContentModule);

container.bind<Program>(Program).to(Program);

export const DI = container;