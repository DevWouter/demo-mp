import { describe, it } from 'mocha';
import { expect } from 'chai';

import { ContentService } from './content.service';
import { Content } from './content';

describe('ContentService', () => {
  let service: ContentService;
  let song1: Content;
  let song2: Content;

  beforeEach(() => {
    service = new ContentService();
    song1 = {};
    song2 = {};
  });

  it('should initially have no content', () => {
    expect(service.getAll()).to.be.empty;
  });

  it('should allow adding content', () => {
    service.add(song1);
    expect(service.getAll()).to.have.lengthOf(1);
  });

  it('should remove content using a predicate', () => {
    service.add(song1);
    service.add(song2);

    service.remove(x => x === song1);

    expect(service.getAll()).to.have.lengthOf(1);
    expect(service.getAll()).to.contain(song2);
    expect(service.getAll()).not.to.contain(song1);
  });
});