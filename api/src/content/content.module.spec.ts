import { describe, it } from 'mocha';
import { expect } from 'chai';
import { Container } from 'inversify';
import { ContentModule } from './content.module';
import { ContentService } from './content.service';

describe('ContentModule', () => {
  let container: Container;
  let isBound: (arg) => Chai.Assertion;

  beforeEach(() => {
    container = new Container();
    container.load(ContentModule);

    isBound = (arg) => expect(container.isBound(arg)).to.be.true;
  });

  it('should have binded ContentService', () => {
    isBound(ContentService);
  })

});