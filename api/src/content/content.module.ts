import { ContainerModule } from "inversify";
import { ContentService } from "./content.service";

export const ContentModule = new ContainerModule((bind, unbind, isBound, rebind) => {
  bind<ContentService>(ContentService).to(ContentService);
});
