import { Content } from "./content";

type contentQueryDelegate = (value: Content) => boolean;

export class ContentService {
  private items: Content[] = [];

  getAll() {
    return this.items;
  }

  add(content: Content) {
    this.items.push(content);
  }

  remove(callbackFn: contentQueryDelegate) {
    this.items = this.items.filter(item => !callbackFn(item));
  }
}