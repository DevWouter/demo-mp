import { DI } from "./inversify.config";
import { Program } from "./program";

const program = DI.get<Program>(Program);
program.run();