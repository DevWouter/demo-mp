import { describe, it } from 'mocha';
import { expect } from 'chai';

import { Program } from './program';
import { ContentService } from './content/content.service';

describe('Program', () => {
  let program: Program;

  beforeEach(() => {
    program = new Program();
  });

  it('should have a run function', () => {
    program.run();
  });


});