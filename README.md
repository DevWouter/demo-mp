# Readme

This repository contains a simple music player for demo purposes.

## How to run

```bat
cd api
npm run start
```

## How to test

```bat
cd api
npm run test:coverage:watch
```